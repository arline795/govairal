import NotFound from './NotFound';
import MainPage from './MainPage';
import Access from './Access';

export const BASEROUTE = '/';

export const ROUTE_MAIN_PAGE = '/main';
export const ROUTE_LOGIN = '/access';
export const ROUTE_LOGOUT = '/logout';

export const routes = [
  {
    path: BASEROUTE,
    component: MainPage,
    exact: true,
    header: true,
    ignoreSession: false,
  },
  {
    path: ROUTE_MAIN_PAGE,
    component: MainPage,
    exact: false,
    header: true,
    ignoreSession: false,
  },
  {
    path: ROUTE_LOGIN,
    component: Access,
    exact: false,
    header: false,
    ignoreSession: true,
  },
  {
    component: NotFound,
    header: false,
  },
];
