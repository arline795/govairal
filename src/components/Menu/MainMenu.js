import React from 'react';

class MainMenu extends React.Component {
  render() {
    return (
      <section className='MainMenu'>
        <div className='columns is-mobile is-centered'>
          <div className='column has-text-centered is-menu'>
            <i className='fas fa-chart-bar' />
            <p className='Font--Menu'>RANKING</p>
          </div>
          <div className='column has-text-centered is-menu'>
            <i className='far fa-edit' />
            <p
              className='Font--Menu'
              style={{
                minWidth: '80px',
              }}
            >
              MIS RETOS
            </p>
          </div>
          <div className='column has-text-centered is-menu active'>
            <figure className='image is-24x24 has_MarginAuto'>
              <img src='assets/image/menu.png' />
            </figure>
          </div>
          <div className='column has-text-centered is-menu'>
            <i className='far fa-bell' />
            <p className='Font--Menu'>NOTIFICACIONES</p>
          </div>
          <div className='column has-text-centered is-menu'>
            <i className='far fa-user' />
            <p className='Font--Menu'>Perfil</p>
          </div>
        </div>
      </section>
    );
  }
}

export default MainMenu;
